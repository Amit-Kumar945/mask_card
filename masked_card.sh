#!/bin/bash
mask_card() {
	card_detail=$1
	digit=$2
	
	if [ "$digit" == "first" ]; then
		awk -F " " '{print "**** "$2" "$3" " $4}' $card_detail
	elif [ "$digit" == "second" ];then
		awk -F " " '{print $1 " **** "$3" " $4}' $card_detail
	elif [ "$digit" == "third" ];then
                awk -F " " '{print $1" " $2 " **** " $4}' $card_detail
	elif [ "$digit" == "four" ];then
                awk -F " " '{print $1" "$2" " $3 " ****"}' $card_detail
	else
		awk -F " " '{print "**** **** **** ****"}' $card_detail
	fi
}


